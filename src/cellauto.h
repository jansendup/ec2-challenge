#ifndef CELLAUTO_H_
#define CELLAUTO_H_

#include <algorithm>
#include <cstdint>
#include <unordered_set>
#include <vector>

template <class T>
bool in_set(T const& set, typename T::key_type const& e) {
  return set.find(e) != set.end();
}

template <class T>
void set_union(T& rhs, T const& lhs) {
  rhs.insert(lhs.cbegin(), lhs.cend());
}

class CARules {
 public:
  CARules() {
    auto const N = 1 << 9;
    rules.reserve(N);
    std::size_t i;
    while ((i = rules.size()) < N) {
      auto x = __builtin_popcount(i & ~(1 << 4));
      if (x == 1 || x == 2) {
        rules.push_back(true);
      } else {
        rules.push_back(false);
      }
    }
  }

  bool operator[](std::size_t i) const { return rules[i]; }

 private:
  std::vector<bool> rules;
};

template <unsigned N>
class CAState {
 private:
  using T = uint32_t;

 public:
  struct Cache {
    unsigned ZERO_PADDED_N;
    unsigned ROW_MASK;

    Cache() {
      ZERO_PADDED_N = N + 2;
      ROW_MASK = (1 << N) - 1;
    }
  };

  class Iterator {
   public:
    Iterator() {}
    Iterator(T value) : value(value) {}

    Iterator begin() { return Iterator(0); }
    Iterator end() { return Iterator(1 << N * N); }

    CAState operator*() { return CAState(value); }
    Iterator& operator++() {
      ++value;
      return *this;
    }

    bool operator!=(Iterator const& other) { return value != other.value; }
    bool operator==(Iterator const& other) { return value == other.value; }

   private:
    T value;
  };

  struct Hash {
    std::size_t operator()(CAState const& s) const { return s.value; }
  };

  CAState() {}
  CAState(T value) : value(value) {}

  static Iterator Permutations() { return Iterator(0); }

  bool IsEmpty() const { return value == 0; }

  void Step(CARules const& rules, Cache const* cache) {
    std::vector<T> rows;
    rows.reserve(cache->ZERO_PADDED_N);
    rows.push_back(0);
    for (auto i = 0; i < N; ++i) {
      rows.push_back((value >> i * N & cache->ROW_MASK) << 1);
    }
    rows.push_back(0);

    value = 0;
    for (auto r = 0; r < N; ++r) {
      for (auto c = 0; c < N; ++c) {
        auto top = rows[r + 2] >> c & 0b111;
        auto mid = rows[r + 1] >> c & 0b111;
        auto bot = rows[r] >> c & 0b111;
        auto index = bot | mid << 3 | top << 6;
        if (rules[index]) {
          value |= 1 << (c + r * N);
        }
      }
    }
  }

 public:
  T value;
};

template <unsigned N>
bool operator==(CAState<N> const& lhs, CAState<N> const& rhs) {
  return lhs.value == rhs.value;
}

template <unsigned N>
class CASimulation {
 public:
  CASimulation() {}

  void NextState(CAState<N>* current) { current->Step(rules, &cache); }
  virtual void Simulate() {}

 private:
  CARules const rules;
  typename CAState<N>::Cache const cache;
};

template <unsigned N>
class CAExtinctionSimulation : public CASimulation<N> {
  using StateSet = std::unordered_set<CAState<N>, typename CAState<N>::Hash>;

 public:
  void Simulate() {
    extinction_set.clear();
    survival_set.clear();

    // For each possible initial state, run a simulation to check if it should
    // be included in the extinction or survival set
    StateSet trail;  // All the states visited within a simulation.
    for (auto ca : CAState<N>::Permutations()) {
      trail.clear();
      while (true) {
        if (in_set(extinction_set, ca) or ca.IsEmpty()) {
          // If we reached a state that is either already in the extinction set
          // or the state itself is an extinct state, add all the states we
          // visited in this simulation to the extinction set
          set_union(extinction_set, trail);
          break;
        } else if (in_set(survival_set, ca) or in_set(trail, ca)) {
          // If we reached a state that is either already in the survival set
          // or is a state that we already visited in this simulation, add all
          // the states we visited in this simulation to the survival set
          set_union(survival_set, trail);
          break;
        } else {
          // Else just add this state to trail (visited list) and continue with
          // simulation
          trail.insert(ca);
          this->NextState(&ca);
        }
      }
    }
  }

  std::size_t ExtinctionCount() { return extinction_set.size(); }
  std::size_t SurvivalCount() { return survival_set.size(); }

 private:
  StateSet extinction_set, survival_set;
};

#endif  // CELLAUTO_H_
