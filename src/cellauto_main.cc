#include <cstdint>
#include <iostream>

#include "cellauto.h"

int main(int argc, char* args[]) {
  CAExtinctionSimulation<5> sim;
  sim.Simulate();
  std::cout << sim.SurvivalCount() << std::endl;
  return 0;
}
