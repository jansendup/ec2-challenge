#include "amazon_strings.h"

#include <iostream>

int main(int argc, char* args[]) {
  AmazonStrings as("AMAZON", 25);
  as.Calculate();
  std::cout << as.OccurrenceCount(1) << std::endl;
}
