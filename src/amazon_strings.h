#ifndef AMAZON_STRINGS_H_
#define AMAZON_STRINGS_H_

#include <algorithm>
#include <cassert>
#include <iostream>
#include <set>
#include <string>
#include <vector>

namespace {

template <class T>
T pow(T x, unsigned n) {
  T y = 1;
  for (; n > 0; --n) y *= x;
  return y;
}

template <class T>
T factorial(unsigned i) {
  T f = 1;
  for (; i > 1; --i) f *= i;
  return f;
}

template <class T>
T combinations(unsigned n, unsigned k) {
  T num = 1;
  auto n_ = n - k;
  for (; n > n_; --n) num *= n;
  return num / factorial<T>(k);
}
}

class AmazonStrings {
 public:
  using Int = long long;
  AmazonStrings(std::string word, Int stringLength)
      : stringLength{stringLength},
        wordLength{static_cast<Int>(word.size())},
        occurrenceCounts(stringLength / static_cast<Int>(word.size())),
        calculated{false} {
    std::set<decltype(word)::value_type> charset{word.begin(), word.end()};
    n = charset.size();
  }

  void Calculate() {
    int maxRepetitions = occurrenceCounts.size();  // stringLength / wordLength

    for (int i = 1; i <= maxRepetitions; ++i) {
      // Calculate how many strings will contain the word at least i times
      Int freeSpaces = stringLength - i * wordLength;
      Int freeLettersPermutations = pow(n, freeSpaces);
      Int shufflePermutations =
          combinations<Int>(stringLength - (wordLength - 1) * i, i);
      occurrenceCounts[i - 1] = freeLettersPermutations * shufflePermutations;
    }

    for (int i = maxRepetitions - 1; i >= 1; --i) {
      // Calculate how many strings will contain the word exactly i times
      for (std::size_t j = i + 1; j <= maxRepetitions; ++j)
        occurrenceCounts[i - 1] -=
            combinations<Int>(j, j - i) * occurrenceCounts[j - 1];
    }
    calculated = true;
  }

  void BruteForce() {
    std::string word;
    word.resize(wordLength);
    std::string::value_type overflowChar = 'a';
    std::fill(word.begin(), word.begin() + (wordLength - n), 'a');
    std::generate(word.begin() + (wordLength - n), word.end(),
                  [&overflowChar] { return overflowChar++; });
    std::fill(occurrenceCounts.begin(), occurrenceCounts.end(), 0);

    std::string perm(stringLength, 'a');
    Int iEnd = pow<Int>(n, stringLength);
    for (Int i = 0; i < iEnd; ++i) {
      auto count = 0;
      auto idx = 0;
      std::size_t pos;
      while ((pos = perm.find(word, idx)) != std::string::npos) {
        idx = pos + wordLength;
        ++count;
      }
      if (count > 0) {
        ++occurrenceCounts[count - 1];
      }

      auto j = 0;
      while (++perm[j] >= overflowChar) {
        perm[j++] = 'a';
      }
    }
    calculated = true;
  }

  Int OccurrenceCount(std::size_t numRepetitions) {
    assert(calculated);
    if (numRepetitions == 0) return 0;
    if (numRepetitions > occurrenceCounts.size()) return 0;
    return occurrenceCounts[numRepetitions - 1];
  }

 private:
  Int stringLength, n, wordLength;
  std::vector<Int> occurrenceCounts;
  bool calculated;
};

#endif
