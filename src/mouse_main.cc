#include "mouse.h"

#include <iomanip>
#include <iostream>
#include <vector>

int main(int argc, char* args[]) {
  MouseMarkovModel mmm(50);
  std::vector<MouseMarkovModel::Action> policy;
  std::vector<MouseMarkovModel::Float> values;
  mmm.FindOptimalPolicy(&values, &policy);
  double e = 1 - values[0];
  std::cout << std::fixed << std::setprecision(5) << e << std::endl;
}
