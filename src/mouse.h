#ifndef MOUSE_H_
#define MOUSE_H_

#include <array>
#include <iostream>
#include <limits>
#include <map>
#include <set>
#include <utility>
#include <vector>

#include "Eigen/Dense"
#include "Eigen/Sparse"

class MouseMarkovModel {
 public:
  enum class Action { BANK = 0, ROLL, LENGTH };

  using Float = double;
  struct Link {
    std::size_t target;
    mutable Float probability, expectedReward;
  };
  using Score = int;
  struct Node {
    Score const banked;
    Score const accumulated;
    struct LinksWrapper {
      struct Compare {
        bool operator()(Link const& a, Link const& b) const {
          return a.target < b.target;
        }
      };
      using Links = std::set<Link, Compare>;
      std::array<Links, (int)Action::LENGTH> links;
      inline Links& operator[](Action const index) {
        return links[static_cast<const int>(index)];
      }
    } links;

    Node(Score const banked, Score const accumulated)
        : banked{banked}, accumulated{accumulated} {}
  };

  using Nodes = std::vector<Node>;
  using NodeIndex = std::pair<Score, Score>;
  using NodeIndexMap = std::map<NodeIndex, std::size_t>;

  struct ScoreProbability {
    Score score;
    Float probability;
  };
  using ScoringTable = std::vector<ScoreProbability>;

  /* Returns the default scoring rules / scoring table*/
  static ScoringTable DefaultScoringTable() {
    return ScoringTable{{0, 0.25}, {2, 0.25}, {3, 0.25}, {4, 0.25}};
  }

  /* Constructs a markov model for a game of 'Mouse' for the give set of scoring
   * rules.*/
  MouseMarkovModel(Score targetScore,
                   ScoringTable scoringTable = DefaultScoringTable())
      : targetScore{targetScore}, scoringTable{scoringTable} {
    CreateNode(0, 0);
  }

  /* Creates node and corresponding links. If a node on the other end of a link
   * doesn't exist then a recursive call to this function will be made to create
   * it. Caller must ensure that the node to be created does not yet exist
   * within the graph.
   *
   * Returns created node's index*/
  std::size_t CreateNode(Score const banked, Score const accumulated) {
    assert(nodeIndexMap.find(NodeIndex{banked, accumulated}) ==
           nodeIndexMap.end());
    // Create node and add node to registry
    auto nodeIndex = nodes.size();
    nodes.push_back(Node{banked, accumulated});
    nodeIndexMap[NodeIndex{banked, accumulated}] = nodeIndex;

    // Check if this node is an exit node
    if (banked + accumulated >= targetScore) {
      return nodeIndex;
    }

    auto CreateLink = [this, nodeIndex](
        Action const& action, Score const& banked, Score const& accumulated,
        Float probability, Float reward) {
      // Find other node and create it if nessasary.
      auto otherIndex = (banked + accumulated < targetScore)
                            ? GetNodeIndex(banked, accumulated)  // Target node
                            : GetNodeIndex(targetScore, 0);      // End node
      auto& links = nodes[nodeIndex].links[action];
      Link link{otherIndex, probability, reward * probability};
      auto linkPtr = links.find(link);
      if (linkPtr == links.end()) {
        // If the link doesn't exist yet insert it
        links.insert(link);
      } else {
        // If the link exist just update it
        linkPtr->probability += link.probability;
        linkPtr->expectedReward += link.expectedReward;
      }
    };
    // Create the edges for this node given a policy to ROLL in this state.
    for (auto& entry : scoringTable) {
      // Find score and reward for die roll
      Score accumulation;
      Float reward;
      if (entry.score == 0) {
        accumulation = 0;
        reward = -1;  // If no points were scored you advance to the next round.
      } else {
        accumulation = accumulated + entry.score;
        reward = 0;
      }

      // Create new edge for this roll
      CreateLink(Action::ROLL, banked, accumulation, entry.probability, reward);
    }

    // Create edge for bank action:
    //   Bank action adds accumulation to the banked points and sets the
    //   accumulated score back to zero.
    CreateLink(Action::BANK, banked + accumulated, 0, 1, -1);

    return nodeIndex;
  }

  /* Gets the index within the graph for given node. If the node doesn't yet
   * exist, it will be created and corresponding index will be returned.*/
  std::size_t GetNodeIndex(Score const banked, Score const accumulated) {
    auto nodeItr = nodeIndexMap.find(NodeIndex{banked, accumulated});
    if (nodeItr == nodeIndexMap.end()) {
      return CreateNode(banked, accumulated);
    }
    return nodeItr->second;
  }

  /* Returns the number of states/nodes within the graph. */
  std::size_t GetNumberOfStates() const { return nodes.size(); }

  /* Evaluates the given policy. Returns each state/node's expected reward,
   * which is equivalent to negative the expected number of rounds to finish.
   */
  void EvalPolicy(std::vector<Action> const& policy,
                  std::vector<Float>* values) {
    assert(values != nullptr);
    assert(nodes.size() == policy.size() && nodes.size() == values->size());

    auto const N = nodes.size();
    using Vec = Eigen::Matrix<Float, Eigen::Dynamic, 1>;
    using VecMap = Eigen::Map<Vec>;
    using Mat = Eigen::SparseMatrix<Float>;

    // Build transition matrix T and reward matrix R for the given policy.
    Mat T(N, N), R(N, N);
    using MatEntry = Eigen::Triplet<Float>;
    std::vector<MatEntry> Tt, Rt;
    auto i = 0;
    auto iI = nodes.begin();
    for (auto& action : policy) {
      for (auto& link : iI->links[action]) {
        auto j = link.target;
        Tt.push_back(MatEntry{i, j, link.probability});
        Rt.push_back(MatEntry{i, j, link.expectedReward});
      }
      ++iI;
      ++i;
    }
    T.setFromTriplets(Tt.begin(), Tt.end());
    R.setFromTriplets(Rt.begin(), Rt.end());

    // Setup linear system Ax = b where A = (I - T) and b = R * [1, 1, ..., 1]`
    Vec b = R * Vec::Constant(N, 1, 1);
    VecMap x(values->data(), N);  // Map 'x' to output vector 'values'
    Mat I(N, N), A(N, N);
    I.setIdentity();
    A = I - T;

    // Solver linear system
    Eigen::SparseLU<Mat> solver;
    solver.compute(A);
    x = solver.solve(b);
  }

  /* Calculate a new optimal policy using the expected reward values of another
   * policy. Returns the new optimal policy and the number of actions that
   * changed between the previous and new policy.*/
  unsigned UpdatePolicy(std::vector<Float> const& values,
                        std::vector<Action>* policy) {
    assert(values.size() == policy->size());

    unsigned changes = 0;
    std::size_t i = 0;
    for (auto& action : *policy) {
      // Find the action that maximizes the expected value at node i.
      Action bestAction = Action::ROLL;
      Float maxExpectation = std::numeric_limits<Float>::lowest();
      for (Action a : std::array<Action, 2>{Action::BANK, Action::ROLL}) {
        auto& links = nodes[i].links[a];
        if (links.size() == 0) continue;

        // Calculate the expected value for action a at node i
        Float expectation = 0;
        for (auto const& link : links) {
          expectation +=
              link.probability * values[link.target] + link.expectedReward;
        }
        if (expectation > maxExpectation) {
          bestAction = a;
          maxExpectation = expectation;
        }
      }

      if (action != bestAction) {
        ++changes;
        action = bestAction;
      }
      ++i;
    }
    return changes;
  }

  /* Initializes a policy using a simple heuristic: At each node find the action
   * that would maximize the expected score accumulation. */
  void InitPolicy(std::vector<Action>* policy) {
    Float a = 0, b;
    for (auto& entry : scoringTable) {
      if (entry.score == 0) {
        b = entry.probability;
      } else {
        a += entry.score * entry.probability;
      }
    }

    auto policyI = policy->begin();
    auto nodesI = nodes.begin();
    for (; policyI != policy->end(); ++policyI, ++nodesI) {
      // If the expeced score accumulation for rolling action in given state is
      // greater than zero then we should probaly roll else we should bank our
      // accumulated points
      *policyI =
          (a - b * nodesI->accumulated > 0) ? Action::ROLL : Action::BANK;
    }
  }

  /* Find the optimal policy that maximizes the expected reward at each node in
   * the graph using policy iteration*/
  void FindOptimalPolicy(std::vector<Float>* values,
                         std::vector<Action>* policy) {
    auto N = nodes.size();
    values->resize(N);
    policy->resize(N);

    InitPolicy(policy);

    do {
      EvalPolicy(*policy, values);
    } while (UpdatePolicy(*values, policy));
  }

 private:
  ScoringTable scoringTable;
  Score targetScore;
  Nodes nodes;
  NodeIndexMap nodeIndexMap;
};

#endif  // MOUSE_H_
