
file(GLOB tests_src "*_test.cc") 

add_executable(tests ${tests_src} ${SOURCES} ${HEADERS})
add_dependencies(tests googletest eigen)
target_link_libraries(tests pthread gtest gtest_main)
set_property(TARGET tests PROPERTY CXX_STANDARD 11)
