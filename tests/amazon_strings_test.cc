#include <gtest/gtest.h>

#include "../src/amazon_strings.h"

namespace {
TEST(StringsTest, SampleCase) {
  AmazonStrings as{"AMAZON", 13};
  as.Calculate();
  EXPECT_EQ(624970, as.OccurrenceCount(1));
}

TEST(StringsTest, DeepRecursion) {
  AmazonStrings calc{"AAB", 12}, bf{"AAB", 12};
  calc.Calculate();
  bf.BruteForce();
  for (auto i = 1; i < 4; ++i) {
    EXPECT_EQ(calc.OccurrenceCount(i), bf.OccurrenceCount(i)) << " i = " << i;
  }

  calc = AmazonStrings{"AAB", 13}, bf = AmazonStrings{"AAB", 13};
  calc.Calculate();
  bf.BruteForce();
  for (auto i = 1; i < 4; ++i) {
    EXPECT_EQ(calc.OccurrenceCount(i), bf.OccurrenceCount(i)) << " i = " << i;
  }

  calc = AmazonStrings{"AB", 8}, bf = AmazonStrings{"AB", 8};
  calc.Calculate();
  bf.BruteForce();
  for (auto i = 1; i < 4; ++i) {
    EXPECT_EQ(calc.OccurrenceCount(i), bf.OccurrenceCount(i)) << " i = " << i;
  }
}
}
