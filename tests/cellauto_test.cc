#include <gtest/gtest.h>

#include "../src/cellauto.h"

namespace {

class CellAutoTest : public ::testing::Test {
 protected:
  CellAutoTest() {}

  virtual ~CellAutoTest() {}

  virtual void SetUp() {}

  virtual void TearDown() {}
};

TEST(CellAutoTest, Step) {
  CASimulation<4> sim;
  CAState<4> ca(0b1000000000000000);
  sim.NextState(&ca);
  EXPECT_EQ(0b0100110000000000, ca.value);
  sim.NextState(&ca);
  EXPECT_EQ(0b0110111011100000, ca.value);
  sim.NextState(&ca);
  EXPECT_EQ(0b0001000000011011, ca.value);
  sim.NextState(&ca);
  EXPECT_EQ(0b0010001111010111, ca.value);
  sim.NextState(&ca);
  EXPECT_EQ(0b0110100010000001, ca.value);
}

TEST(CellAutoTest, Solve) {
  CAExtinctionSimulation<4> sim;
  sim.Simulate();
  EXPECT_EQ(41698, sim.SurvivalCount());
}
}
