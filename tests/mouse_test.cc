#include <gtest/gtest.h>

#include "../src/mouse.h"

namespace {
TEST(MouseTest, EvalPolicy) {
  MouseMarkovModel mmm(20);
  std::vector<MouseMarkovModel::Action> policy;
  auto N = mmm.GetNumberOfStates();
  for (auto i = 0; i < N; ++i) {
    policy.push_back(MouseMarkovModel::Action::ROLL);
  }
  std::vector<MouseMarkovModel::Float> values(N);

  mmm.EvalPolicy(policy, &values);

  EXPECT_NEAR(1 - values[0], 7.40008, 0.5e-5);
}

TEST(MouseTest, FindOptimalPolicy) {
  MouseMarkovModel mmm(20);
  std::vector<MouseMarkovModel::Action> policy;
  std::vector<MouseMarkovModel::Float> values;

  mmm.FindOptimalPolicy(&values, &policy);

  EXPECT_NEAR(1 - values[0], 5.46815, 0.5e-5);
}
}
